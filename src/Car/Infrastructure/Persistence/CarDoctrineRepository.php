<?php

declare(strict_types=1);

namespace App\Car\Infrastructure\Persistence;

use App\Car\Domain\Entities\Car;
use App\Car\Domain\Interfaces\CarReadStorage;
use App\Car\Domain\Interfaces\CarWriteStorage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class CarDoctrineRepository extends ServiceEntityRepository
    implements CarReadStorage, CarWriteStorage
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    public function add(Car $car): void
    {
        $this->getEntityManager()->persist($car);
    }

    public function getByUserId(string $externalId): array
    {
        return $this->findBy([
            'externalId' => $externalId,
            'deletedAt' => null,
        ], ['createdAt' => 'asc']);
    }
}
