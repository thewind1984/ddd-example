<?php

declare(strict_types=1);

namespace App\Car\Infrastructure\Persistence\Mapping;

enum CarNumberType: string
{
    case CITIZEN = 'citizen';
    case POLICE = 'police';
    case TAXI = 'taxi';
    case MOTORBIKE = 'motorbike';
    case DIPLOMATIC = 'diplomatical';
    case MILITARY = 'military';
    case TRANSIT = 'transit';
    case UNKNOWN = 'unknown';
    case OTHER = 'other';
}
