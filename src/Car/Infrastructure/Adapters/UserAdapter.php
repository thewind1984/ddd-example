<?php

declare(strict_types=1);

namespace App\Car\Infrastructure\Adapters;

use App\Car\Domain\Providers\UserProvider;
use App\User\Application\UserQueryProcessor;

final readonly class UserAdapter implements UserProvider
{
    public function __construct(
        private UserQueryProcessor $userQueryProcessor,
    ) {}

    public function getUserNameById(string $userId): ?string
    {
        /*return $this->userQueryProcessor->getUserNameById($userId);*/
        httpClient->get('http://user_domain/api/user/by_name', ['id' => $userId])->getContent();
    }
}
