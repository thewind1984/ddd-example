<?php

declare(strict_types=1);

namespace App\Car\Presentation\Api\Rest\Version1;

use App\Car\Application\CarQueryProcessor;
use App\Car\Domain\Entities\Car;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as Doc;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

#[Route(path: "/cars")]
#[Doc\Tag(name: "car")]
#[Doc\Response(response: 401, description: 'Неверные данные авторизации', content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
#[Doc\Response(response: 404, description: 'Пользователь внешнего ресурса не зарегистрирован', content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
#[Security(name: 'AppClientId')]
final class CarQueryController extends AbstractController
{
    #[Route(methods: ['GET']), Doc\Get(summary: 'Список автомобилей текущего пользователя telegram', responses: [
        new Doc\Response(response: 200, description: 'Список автомобилей текущего пользователя telegram',
            content: new Doc\JsonContent(properties: [
                new Doc\Property(property: 'data', type: 'array', items: new Doc\Items(ref: new Model(type: Car::class, groups: ['public']))),
            ]),
        )
    ])]
    public function listAction(CarQueryProcessor $carQueryProcessor): JsonResponse
    {
        return $this->json([
            'data' => $carQueryProcessor->getCarsByTelegramUserExternalId(
                'some_external_id'
            ),
        ], context: [
            AbstractNormalizer::GROUPS => ['public'],
            AbstractObjectNormalizer::SKIP_NULL_VALUES => false,
        ]
        );
    }
}
