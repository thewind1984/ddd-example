<?php

declare(strict_types=1);

namespace App\Car\Application;

use App\Car\Domain\Entities\Car;
use App\Car\Domain\Interfaces\CarReadStorage;
use App\Car\Domain\Providers\UserProvider;

final readonly class CarQueryProcessor
{
    public function __construct(
        private CarReadStorage $carReadStorage,
        private UserProvider $userProvider,
    ) {}

    public function getCarsByTelegramUserExternalId(string $externalId): array
    {
        $cars = $this->carReadStorage->getByUserId($externalId);

        $userName = $this->userProvider->getUserNameById($externalId);

        return array_map(
            static fn (Car $car): array => [
                'car' => $car,
                'userName' => $userName,
            ],
            $cars
        );
    }
}
