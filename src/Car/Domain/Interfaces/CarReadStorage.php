<?php

declare(strict_types=1);

namespace App\Car\Domain\Interfaces;

use App\Car\Domain\Entities\Car;

interface CarReadStorage
{
    /**
     * @return Car[]
     */
    public function getByUserId(string $externalId): array;
}
