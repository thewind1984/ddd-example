<?php

declare(strict_types=1);

namespace App\Car\Domain\Interfaces;

use App\Car\Domain\Entities\Car;

interface CarWriteStorage
{
    public function add(Car $car): void;
}
