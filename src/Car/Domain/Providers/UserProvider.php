<?php

declare(strict_types=1);

namespace App\Car\Domain\Providers;

interface UserProvider
{
    public function getUserNameById(string $userId): ?string;
}
