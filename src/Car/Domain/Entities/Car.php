<?php

declare(strict_types=1);

namespace App\Car\Domain\Entities;

use App\Car\Infrastructure\Persistence\CarDoctrineRepository;
use App\Car\Infrastructure\Persistence\Mapping\CarNumberType;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Attributes\Property;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CarDoctrineRepository::class)]
#[ORM\Table(name: 'car')]
class Car
{
    #[ORM\Id]
    #[ORM\Column]
    private string $id;

    #[ORM\Column]
    private int $userId;

    #[ORM\Column]
    private string $externaId;

    #[ORM\Column(nullable: true)]
    private ?string $number;

    #[ORM\Column(type: 'string', nullable: true, enumType: CarNumberType::class, options: ['default' => CarNumberType::UNKNOWN])]
    private ?CarNumberType $numberType = CarNumberType::UNKNOWN;

    #[ORM\Column(nullable: true)]
    private ?string $comment;

    #[ORM\Column(name: 'creation_date', type: 'datetime', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private \DateTime $createdAt;

    #[ORM\Column(name: 'deleted_at', type: 'datetime', nullable: true)]
    private ?\DateTime $deletedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public static function create(
        ?int $userId,
        string $number,
        ?string $comment,
    ): self {
        $self = new self;

        $self->userId = $userId;
        $self->number = $number;
        $self->comment = $comment;

        return $self;
    }

    #[Groups('public')]
    public function getId(): string
    {
        return $this->id;
    }

    #[Groups('public')]
    #[Property(example: 'A123BC177')]
    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    #[Groups('public')]
    #[Property(example: 'Главная машина')]
    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function delete(): void
    {
        $this->deletedAt = new \DateTime();

        // hack for mysql (it cannot apply constraints with where conditions)
        $this->id .= '-deleted-' . $this->deletedAt->getTimestamp();    // to be unique
    }
}
