<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Persistence\Doctrine\Functions\Json;

/**
 * "JSON_GET_TEXT" "(" StringPrimary "," AlphaNumeric ")"
 */
final class JsonGetText extends JsonOperatorFunctionNode
{
    public const FUNCTION_NAME = 'JSON_GET_TEXT';
    public const OPERATOR = '->>';

    /** @var string[] */
    protected array $requiredArgumentTypes = [self::STRING_PRIMARY_ARG, self::VALUE_ARG];
}
