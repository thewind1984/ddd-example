<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Persistence\Doctrine\Functions\Json;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\ORM\Query\SqlWalker;

abstract class JsonOperatorFunctionNode extends AbstractJsonOperatorFunctionNode
{
    public const OPERATOR = null;

    /**
     * @throws Exception
     */
    protected function validatePlatform(SqlWalker$sqlWalker): void
    {
        if (!$sqlWalker->getConnection()->getDatabasePlatform() instanceof PostgreSQLPlatform) {
            throw Exception::notSupported(static::FUNCTION_NAME);
        }
    }

    public function getOperator(): string
    {
        return static::OPERATOR;
    }
}
