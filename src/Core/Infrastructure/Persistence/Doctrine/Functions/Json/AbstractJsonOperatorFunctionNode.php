<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Persistence\Doctrine\Functions\Json;

abstract class AbstractJsonOperatorFunctionNode extends AbstractJsonFunctionNode
{
    /** @var string[] */
    protected array $requiredArgumentTypes = [self::STRING_PRIMARY_ARG, self::STRING_PRIMARY_ARG];

    abstract public function getOperator(): string;

    /**
     * @param string[] $arguments
     */
    protected function getSqlForArgs(array $arguments): string
    {
        [$leftArg, $rightArg] = $arguments;
        return sprintf('%s %s %s', $leftArg, $this->getOperator(), $rightArg);
    }
}
