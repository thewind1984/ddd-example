<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Persistence\Doctrine\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

final class ConcatWs extends FunctionNode
{
    public const STRINGS_KEY = 'strings';
    public const SEPARATOR_KEY = 'separator';

    private array $parameters = [];

    public function getSql(SqlWalker $sqlWalker): string
    {
        return  'CONCAT_WS(' . $this->parameters[self::SEPARATOR_KEY]->dispatch($sqlWalker) . ','
            . implode(',', array_map(function ($str) use ($sqlWalker) {
                return $str->dispatch($sqlWalker);
            }, $this->parameters[self::STRINGS_KEY])) . ')';
    }

    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->parameters[self::SEPARATOR_KEY] = $parser->StringPrimary();

        $parser->match(Lexer::T_COMMA);

        $this->parameters[self::STRINGS_KEY][] = $parser->StringPrimary();

        while ($parser->getLexer()->isNextToken(Lexer::T_COMMA)) {
            $parser->match(Lexer::T_COMMA);
            $this->parameters[self::STRINGS_KEY][] = $parser->StringPrimary();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
