<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Transformers;

use App\Core\Domain\Exceptions\NonTransformableRequestException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsEventListener(event: "kernel.request", method: "onKernelRequest", priority: 100)]
#[AsEventListener(event: "kernel.exception", method: "onKernelException", priority: 100)]
final readonly class JsonTransformerListener
{
    public function __construct(
        private TranslatorInterface $translator,
        private LoggerInterface $logger,
        private string $appEnv
    ) {}

    /**
     * @throws NonTransformableRequestException
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if (empty($request->getContent())) {
            return;
        }

        if (!$this->isJsonRequest($request)) {
            return;
        }

        $this->transformJsonBody($request);
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        if (preg_match('#^/((?:en|ru)/)?api/v\d+#ui', $event->getRequest()->getPathInfo()) !== 1) {
            return;
        }

        $exception = $event->getThrowable();
        $code = $exception->getCode() < 100 ? Response::HTTP_BAD_REQUEST : $exception->getCode();
        $exceptionClassParts = explode('\\', get_class($exception));
        $exceptionCode = end($exceptionClassParts);

        $data = [
            'code' => $exceptionCode,
        ];

        match (true) {
            // here we can add extra keys to $data object depending on $exception instance
            default => null,
        };

        $exceptionMessage = trim($exception->getMessage());
        if (!$exceptionMessage) {
            $exceptionSnakeCaseCode = preg_replace_callback('/(?<s>[a-z]?)(?<b>[A-Z])/', function (array $match): string {
                return ($match['s'] !== '' ? ($match['s'] . '_') : '') . strtolower($match['b']);
            }, $exceptionCode);

            $exceptionMessage = $this->translator->trans($exceptionSnakeCaseCode, array_combine(
                array_map(static fn (string $key): string => '{{ ' . $key . ' }}', array_keys($data)),
                array_values($data)
            ), 'exceptions');
        }

        $data['message'] = $exceptionMessage;

        match (true) {
            $exception instanceOf UniqueConstraintViolationException => $data['message'] = $this->convertUniqueViolationToCustomMessage($exceptionMessage),
            default => null,
        };

        match (true) {
            $exception instanceOf UniqueConstraintViolationException => $code = Response::HTTP_CONFLICT,
            default => null,
        };

        $this->logger->error('ResponseException', $data);

        if ($this->appEnv !== 'prod') {
            $data['dev_debug'] = [
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTrace(),
            ];
        }

        $response = new JsonResponse($data, $code);
        $event->setResponse($response);
    }

    private function isJsonRequest(Request $request): bool
    {
        return $request->getContentTypeFormat() === 'json';
    }

    /**
     * @throws NonTransformableRequestException
     */
    private function transformJsonBody(Request $request): void
    {
        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NonTransformableRequestException(json_last_error_msg());
        }

        if ($data === null) {
            return;
        }

        $request->request->replace($data);
    }

    private function convertUniqueViolationToCustomMessage(string $message): string
    {
        preg_match('/Key \((?<key>[^)]+)\)=\((?<value>.+)\) already/ui', $message, $matches);

        return $this->translator->trans('unique_constraint_violation_exception', [
            '{{ key }}' => $matches['key'],
            '{{ value }}' => $matches['value'],
        ], 'exceptions');
    }
}
