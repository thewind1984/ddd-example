<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Services;

final readonly class Timezone
{
    public const SQL_TIME_FORMAT = 'Y-m-d H:i:s P';

    public static function getTz(): \DateTimeZone
    {
        return new \DateTimeZone(getallheaders()['Client-Timezone'] ?? ini_get('date.timezone'));
    }

    public static function now(?\DateTimeZone $tz = null, ?string $format = null): \DateTimeImmutable|string
    {
        $stamp = self::from(tz: $tz);

        if ($format === null) {
            return $stamp;
        }

        return $stamp->format($format);
    }

    public static function from(?string $stamp = null, ?\DateTimeZone $tz = null): \DateTimeImmutable
    {
        return new \DateTimeImmutable($stamp ?? 'now', $tz ?? self::getTz());
    }

    public static function plainSqlNow(): string
    {
        return self::now(format: self::SQL_TIME_FORMAT);
    }
}
