<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\EventListeners;

use App\Core\Application\SystemClientQueryProcessor;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

#[AsEventListener(event: "kernel.request", method: "checkAppClientId", priority: 99)]
final readonly class AppClientRequestListener
{
    public function __construct(
        private SystemClientQueryProcessor $systemClientQueryProcessor,
    ) {}

    /**
     * @throws \Exception
     */
    public function checkAppClientId(RequestEvent $event): void
    {
        if (!$this->supports($event->getRequest())) {
            return;
        }

        if ($event->getRequest()->headers->get('X-AppClientId')
            !==
            $this->systemClientQueryProcessor->getCurrent()->getSystemName()
        ) {
            throw new \Exception('Wrong AppClient ID');
        }
    }

    private function supports(Request $request): bool
    {
        return (bool)preg_match('#^/api/v\d+/.+#', $request->getRequestUri());
    }
}
