<?php

declare(strict_types=1);

namespace App\Core\Domain\Entities;

use Doctrine\ORM\Mapping\Column;
use Ramsey\Uuid\UuidInterface;

trait CreatedTrait
{
    #[Column(type: "datetimetz_immutable", options: ["default" => "CURRENT_TIMESTAMP"])]
    private \DateTimeImmutable $createdAt;

    #[Column(type: 'uuid', nullable: true)]
    private ?UuidInterface $createdByUserId;

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedByUserId(): ?UuidInterface
    {
        return $this->createdByUserId;
    }
}
