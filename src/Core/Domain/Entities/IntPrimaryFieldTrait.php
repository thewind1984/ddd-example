<?php

declare(strict_types=1);

namespace App\Core\Domain\Entities;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;

trait IntPrimaryFieldTrait
{
    #[Id, Column(type: "bigint", unique: true)]
    #[GeneratedValue(strategy: "IDENTITY")]
    private int $id = 0;

    public function getId(): int
    {
        return $this->id;
    }
}
