<?php

declare(strict_types=1);

namespace App\Core\Domain\Entities;

use App\Core\Infrastructure\Services\Timezone;
use Doctrine\ORM\Mapping\Column;

trait SoftDeleteTrait
{
    #[Column(type: "datetimetz_immutable", nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    #[Column(nullable: true)]
    private ?int $deletedByUserId = null;

    public function delete(?int $userId = null): void
    {
        $this->deletedAt = Timezone::now();
        $this->deletedByUserId = $userId;
    }

    public function isDeleted(): bool
    {
        return $this->deletedAt !== null;
    }

    public function getDeletedByUserId(): ?int
    {
        return $this->deletedByUserId;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }
}
