<?php

declare(strict_types=1);

namespace App\Core\Domain\Exceptions;

final class NonTransformableRequestException extends \Exception
{}
