<?php

declare(strict_types=1);

namespace App\Core\Domain\Models;

use App\Core\Infrastructure\Services\Timezone;

trait ModelDateFormatterTrait
{
    protected function outputDateTime(\DateTimeImmutable|\DateTime|null $stamp, string $format = 'Y-m-d H:i:s T'): ?string
    {
        // since immutable object cannot be modified, we need another variable
        $newStamp = $stamp?->setTimezone(Timezone::getTz());
        return $newStamp?->format($format);
    }
}
