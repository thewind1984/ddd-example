<?php

declare(strict_types=1);

namespace App\User\Application;

use App\User\Domain\Interfaces\UserReadStorage;
use Ramsey\Uuid\Uuid;

final readonly class UserQueryProcessor
{
    public function __construct(
        private UserReadStorage $userReadStorage,
    ) {}

    public function getUserNameById(string $userId): ?string
    {
        $user = $this->userReadStorage->get(
            Uuid::fromString($userId)
        );

        return $user?->getName();
    }
}
