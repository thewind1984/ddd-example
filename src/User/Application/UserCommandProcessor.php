<?php

declare(strict_types=1);

namespace App\User\Application;

use App\User\Domain\Dto\CreateUserDto;
use App\User\Domain\Entities\User;
use App\User\Domain\Events\NewUserCreated;
use App\User\Domain\Exceptions\UserEmptyPasswordException;
use App\User\Domain\Exceptions\UserPasswordAndConfirmationAreDifferentException;
use App\User\Domain\Exceptions\UserVerificationNotFoundException;
use App\User\Domain\Interfaces\UserReadStorage;
use App\User\Domain\Interfaces\UserWriteStorage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final readonly class UserCommandProcessor
{
    public function __construct(
        private EntityManagerInterface $em,
        private UserPasswordHasherInterface $passwordHasher,
        private UserReadStorage $userReadStorage,
        private UserWriteStorage $userWriteStorage,
        private MessageBusInterface $messenger,
    ) {}

    /**
     * @throws UserEmptyPasswordException
     * @throws UserPasswordAndConfirmationAreDifferentException
     */
    public function createUser(CreateUserDto $command): User
    {
        /** @var User $user */
        $user = $this->em->wrapInTransaction(
            function () use ($command): User {
                if (empty($command->password) || trim($command->password) === '') {
                    throw new UserEmptyPasswordException();
                }
                if ($command->password !== $command->passwordConfirmation) {
                    throw new UserPasswordAndConfirmationAreDifferentException();
                }

                $user = User::create($command->email, $command->ip);
                $user->setPassword($this->passwordHasher->hashPassword($user, $command->password));
                $user->requestVerification();

                $this->userWriteStorage->add($user);

                return $user;
            }
        );

        $this->messenger->dispatch(new NewUserCreated($user->getId(), $command->locale));

        return $user;
    }

    public function verifyUserByCode(string $code): void
    {
        $this->em->wrapInTransaction(
            function () use ($code) {
                $userToVerify = $this->userReadStorage->getUnverifiedByCode($code);

                if ($userToVerify === null) {
                    throw new UserVerificationNotFoundException();
                }

                $userToVerify->verify();
            }
        );
    }
}
