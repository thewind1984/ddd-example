<?php

declare(strict_types=1);

namespace App\User\Application;

use App\User\Domain\Dto\CreateJWTTokenDto;
use App\User\Domain\Entities\JWTToken;
use App\User\Domain\Entities\User;
use App\User\Domain\Interfaces\JWTTokenReadStorage;
use App\User\Domain\Interfaces\JWTTokenWriteStorage;
use Doctrine\ORM\EntityManagerInterface;

final readonly class JWTTokenProcessor
{
    public function __construct(
        private EntityManagerInterface $em,
        private JWTTokenReadStorage $JWTTokenReadStorage,
        private JWTTokenWriteStorage $JWTTokenWriteStorage
    ) {}

    public function getByUserAndJWTString(User $user, string $jwtString): ?JWTToken
    {
        return $this->JWTTokenReadStorage->getByUserAndJWTString($user, $jwtString);
    }

    public function invalidateToken(string $jwtString): void
    {
        $this->em->wrapInTransaction(
            function () use ($jwtString): void {
                $this->JWTTokenReadStorage->get($jwtString, true)?->invalidate();
            }
        );
    }

    public function invalidateUserTokens(User $user): void
    {
        $this->em->wrapInTransaction(
            function () use ($user): void {
                $tokens = $this->JWTTokenReadStorage->getByUser($user);
                foreach ($tokens as $token) {
                    $token->invalidate();
                }
            }
        );
    }

    public function createToken(CreateJWTTokenDto $command): JWTToken {
        return $this->em->wrapInTransaction(
            function () use ($command): JWTToken {
                $token = JWTToken::create(
                    $command->user,
                    $command->token,
                    $command->expiresIn,
                    $command->clientIp,
                    $command->userAgent
                );
                $this->JWTTokenWriteStorage->add($token);

                return $token;
            }
        );
    }
}
