<?php

declare(strict_types=1);

namespace App\User\Domain\Exceptions;

use Symfony\Component\HttpFoundation\Response;

final class UserVerificationNotFoundException extends \Exception
{
    protected $code = Response::HTTP_NOT_FOUND;
}
