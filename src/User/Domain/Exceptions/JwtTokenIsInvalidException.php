<?php

declare(strict_types=1);

namespace App\User\Domain\Exceptions;

use Symfony\Component\HttpFoundation\Response;

final class JwtTokenIsInvalidException extends \Exception
{
    protected $code = Response::HTTP_UNAUTHORIZED;
}
