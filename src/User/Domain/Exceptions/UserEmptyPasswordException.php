<?php

declare(strict_types=1);

namespace App\User\Domain\Exceptions;

final class UserEmptyPasswordException extends \Exception
{}
