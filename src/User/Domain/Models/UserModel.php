<?php

declare(strict_types=1);

namespace App\User\Domain\Models;

use App\Core\Domain\Models\ModelDateFormatterTrait;
use App\User\Domain\Entities\User;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;
use Symfony\Component\Security\Core\User\UserInterface;

final readonly class UserModel
{
    use ModelDateFormatterTrait;

    public string $id;
    public ?string $email;
    #[Property(type: "array", items: new Items(type: "string"))]
    public array $roles;
    public string $registeredAt;
    public ?string $name;

    public function __construct(User|UserInterface $user)
    {
        $this->id = $user->getId()->toString();
        $this->email = $user->getEmail();
        $this->roles = $user->getRoles();
        $this->registeredAt = $this->outputDateTime($user->getCreatedAt());
        $this->name = $user->getName();
    }
}
