<?php

declare(strict_types=1);

namespace App\User\Domain\Events;

use Ramsey\Uuid\UuidInterface;

final readonly class NewUserCreated
{
    public function __construct(
        public UuidInterface $userId,
        public string $locale,
    ) {}
}
