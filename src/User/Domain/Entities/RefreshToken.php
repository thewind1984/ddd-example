<?php

declare(strict_types=1);

namespace App\User\Domain\Entities;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken as BaseRefreshToken;

/**
 * Токены для обновления основных JWT токенов (refresh-токены)
 */
#[Entity]
#[Table(name: "refresh_token")]
class RefreshToken extends BaseRefreshToken
{
}
