<?php

declare(strict_types=1);

namespace App\User\Domain\Entities;

use App\Core\Domain\Entities\CreatedTrait;
use App\Core\Domain\Entities\SoftDeleteTrait;
use App\Core\Domain\Entities\UuidPrimaryFieldTrait;
use App\Core\Infrastructure\Services\Timezone;
use App\User\Domain\Exceptions\UserEmailInvalidException;
use App\User\Infrastructure\Persistence\Mapping\UserStatusType;
use App\User\Infrastructure\Persistence\Mapping\UserTypeType;
use App\User\Infrastructure\Persistence\UserDoctrineRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[Entity(repositoryClass: UserDoctrineRepository::class)]
#[Table(name: "users")]
#[UniqueConstraint(name: "user_email", columns: ["email"], options: ['where' => 'deleted_at IS NULL'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use UuidPrimaryFieldTrait;
    use CreatedTrait;
    use SoftDeleteTrait;

    #[Column(nullable: true)]
    #[Assert\Email]
    private ?string $email = null;

    #[Column(nullable: true)]
    private ?string $password = null;

    #[Column(type: "string", enumType: UserTypeType::class, options: ['default' => UserTypeType::USER])]
    private UserTypeType $type = UserTypeType::USER;

    #[Column(type: "string", enumType: UserStatusType::class, options: ['default' => UserStatusType::NEW])]
    private UserStatusType $status = UserStatusType::NEW;

    #[Column(nullable: true)]
    private ?string $verificationCode = null;

    #[Column(type: "datetimetz_immutable", nullable: true)]
    private ?\DateTimeImmutable $verifiedAt = null;

    #[Column(length: 50)]
    private string $registeredIp;

    #[Column(type: "datetimetz_immutable", nullable: true)]
    private ?\DateTimeImmutable $lastLoginAt = null;

    #[Column(nullable: true)]
    private ?string $name = null;

    #[OneToMany(mappedBy: 'user', targetEntity: JWTToken::class, cascade: ['all'])]
    private Collection $tokens;

    public function __construct()
    {
        $this->createdAt = Timezone::now();
    }

    /**
     * @throws UserEmailInvalidException
     */
    public static function create(string $email, string $ip): self
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new UserEmailInvalidException();
        }

        $self = new self;

        $self->email = $email;
        $self->registeredIp = $ip;

        return $self;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getName(): ?string
    {
        return $this->name ?? $this->email;
    }

    public function login(): void
    {
        $this->lastLoginAt = Timezone::now();
    }

    public function getRoles(): array
    {
        return match ($this->type) {
            UserTypeType::USER => ['ROLE_USER'],
            UserTypeType::ADMIN => ['ROLE_ADMIN'],
        };
    }

    #[NoReturn]
    public function eraseCredentials(): void
    {}

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function requestVerification(): void
    {
        $this->verificationCode = hash('sha256', md5($this->email));
    }

    public function getVerificationCode(): ?string
    {
        return $this->verificationCode;
    }

    public function verify(): void
    {
        $this->status = UserStatusType::VERIFIED;
        $this->verificationCode = null;
        $this->verifiedAt = Timezone::now();
    }

    public function isVerified(): bool
    {
        return $this->status === UserStatusType::VERIFIED;
    }
}
