<?php

declare(strict_types=1);

namespace App\User\Domain\Entities;

use App\Core\Domain\Entities\IntPrimaryFieldTrait;
use App\Core\Infrastructure\Services\Timezone;
use App\User\Infrastructure\Persistence\JWTTokenDoctrineRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

/**
 * JWT токены пользователей с привязкой к IP и user-agent
 * Позволяет "убить" все токены пользователя разом, обновив поле expiresAt
 */
#[Entity(repositoryClass: JWTTokenDoctrineRepository::class)]
#[Table(name: "user_jtw_token")]
class JWTToken
{
    use IntPrimaryFieldTrait;

    #[Column(type: "text")]
    private string $token;

    #[ManyToOne(targetEntity: User::class, cascade: ['all'], inversedBy: 'tokens')]
    private User $user;

    #[Column(type: "datetimetz_immutable")]
    private \DateTimeImmutable $issuedAt;

    #[Column(type: "datetimetz_immutable", nullable: true)]
    private ?\DateTimeImmutable $expiresAt;

    #[Column(nullable: true)]
    private ?string $clientIp;

    #[Column(nullable: true)]
    private ?string $userAgent;

    public function __construct()
    {
        $this->issuedAt = Timezone::now();
    }

    public static function create(
        User $user,
        string $token,
        ?int $expiresIn = null,
        ?string $clientIp = null,
        ?string $userAgent = null
    ): self {
        $self = new self;

        $self->user = $user;
        $self->token = $token;
        $self->expiresAt = $expiresIn !== null ? (Timezone::now())->add(new \DateInterval('PT' . $expiresIn . 'S')) : null;
        $self->clientIp = $clientIp;
        $self->userAgent = $userAgent;

        return $self;
    }

    public function invalidate(): void
    {
        $this->expiresAt = Timezone::now()->sub(new \DateInterval('PT1S'));
    }

    public function isValid(): bool
    {
        return $this->expiresAt === null || $this->expiresAt > Timezone::now();
    }

    public function getToken(): string
    {
        return $this->token;
    }
}
