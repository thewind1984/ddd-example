<?php

declare(strict_types=1);

namespace App\User\Domain\Dto;

use App\User\Domain\Entities\User;

final readonly class CreateJWTTokenDto
{
    public function __construct(
        public User $user,
        public string $token,
        public ?int $expiresIn = null,
        public ?string $clientIp = null,
        public ?string $userAgent = null
    ) {}
}
