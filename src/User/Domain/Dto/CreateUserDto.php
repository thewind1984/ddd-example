<?php

declare(strict_types=1);

namespace App\User\Domain\Dto;

final readonly class CreateUserDto
{
    public function __construct(
        public string $email,
        public string $password,
        public string $passwordConfirmation,
        public string $ip,
        public string $locale,
    ) {}
}
