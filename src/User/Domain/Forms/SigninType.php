<?php

declare(strict_types=1);

namespace App\User\Domain\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

final class SigninType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'user.form.signin.email',
                'translation_domain' => $options['translation_domain'],
            ])
            ->add('password', PasswordType::class, [
                'label' => 'user.form.signin.password',
                'translation_domain' => $options['translation_domain'],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'user.buttons.signin',
                'translation_domain' => $options['translation_domain'],
            ])
        ;

        parent::buildForm($builder, $options);
    }
}
