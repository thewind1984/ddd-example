<?php

declare(strict_types=1);

namespace App\User\Domain\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

final class SignupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'user.form.signup.email',
                'translation_domain' => $options['translation_domain'],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'user.form.signup.password'],
                'second_options' => ['label' => 'user.form.signup.password_confirmation'],
                'translation_domain' => $options['translation_domain'],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'user.buttons.signup',
                'translation_domain' => $options['translation_domain'],
            ])
        ;

        parent::buildForm($builder, $options);
    }
}
