<?php

declare(strict_types=1);

namespace App\User\Domain\Interfaces;

use App\User\Domain\Entities\JWTToken;

interface JWTTokenWriteStorage
{
    public function add(JWTToken $JWTToken): void;
}
