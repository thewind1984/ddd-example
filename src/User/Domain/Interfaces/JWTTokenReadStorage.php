<?php

declare(strict_types=1);

namespace App\User\Domain\Interfaces;

use App\User\Domain\Entities\JWTToken;
use App\User\Domain\Entities\User;

interface JWTTokenReadStorage
{
    /**
     * @return JWTToken[]
     */
    public function getByUser(User $user): array;

    public function get(string $jwtString, bool $lock = false): ?JWTToken;

    public function getByUserAndJWTString(User $user, string $jwtString): ?JWTToken;
}
