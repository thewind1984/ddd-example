<?php

declare(strict_types=1);

namespace App\User\Domain\Interfaces;

use App\User\Domain\Entities\User;

interface UserWriteStorage
{
    public function add(User $user): void;
}
