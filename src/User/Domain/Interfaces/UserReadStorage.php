<?php

declare(strict_types=1);

namespace App\User\Domain\Interfaces;

use Ramsey\Uuid\UuidInterface;
use App\User\Domain\Entities\User;

interface UserReadStorage
{
    public function get(UuidInterface $id, bool $lock = false): ?User;
    public function getUnverifiedByCode(string $code): ?User;
}
