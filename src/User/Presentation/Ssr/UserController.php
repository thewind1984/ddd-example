<?php

declare(strict_types=1);

namespace App\User\Presentation\Ssr;

use App\User\Application\UserCommandProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

final class UserController extends AbstractController
{
    #[Route('/signin', name: 'user.signin', methods: ["GET", "POST"])]
    public function signInAction(): Response
    {
        if ($this->getUser() !== null) {
            return $this->redirect('/');
        }

        return $this->redirect('/');
    }

    #[Route('/verify/{code}', name: 'user.verify', methods: ["GET"])]
    public function verifyAction(string $code, UserCommandProcessor $commandProcessor): RedirectResponse
    {
        if ($this->getUser() !== null) {
            return $this->redirect('/');
        }

        try {
            $commandProcessor->verifyUserByCode($code);

            $this->addFlash('success', 'user.verification.status.success');

            return $this->redirectToRoute('user.signin');
        } catch (\Throwable $e) {
            $this->addFlash('danger', 'user.verification.status.failed');

            return $this->redirect('/');
        }
    }

    #[Route('/signout', name: 'user.signout', methods: ["GET"])]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function singoutAction(): void
    {}
}
