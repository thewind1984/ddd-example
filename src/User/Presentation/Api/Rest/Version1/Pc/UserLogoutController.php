<?php

declare(strict_types=1);

namespace App\User\Presentation\Api\Rest\Version1\Pc;

use App\User\Application\JWTTokenProcessor;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as Doc;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: "/user")]
#[Doc\Tag(name: "user")]
#[Doc\Response(response: 401, description: "Invalid JWT token", content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
#[Doc\Response(response: 403, description: "Access denied", content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
#[Security(name: "Bearer")]
final class UserLogoutController extends AbstractController
{
    #[Route(path: "/logout", name: "user_logout", methods: ["GET"])]
    #[Doc\Get(summary: "Invalidate user token (or all tokens)", parameters: [
        new Doc\QueryParameter(name: "everywhere", schema: new Doc\Schema(description: "Invalidate all tokens on all devices", type: "bool", default: false))
    ], responses: [
        new Doc\Response(response: 200, description: "Logout was successful", content: new Doc\JsonContent(type: "object"))
    ])]
    public function __invoke(
        Request $request,
        JWTTokenProcessor $tokenProcessor,
    ): JsonResponse {
        if ($request->query->getBoolean('everywhere')) {
            $tokenProcessor->invalidateUserTokens($this->getUser()->getUserIdentifier());
        } // otherwise current token will be invalidated during JWTAuthenticatedEvent

        return $this->json(
            null,
            Response::HTTP_OK
        );
    }
}
