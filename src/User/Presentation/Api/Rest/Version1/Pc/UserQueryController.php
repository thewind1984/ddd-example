<?php

declare(strict_types=1);

namespace App\User\Presentation\Api\Rest\Version1\Pc;

use App\User\Domain\Models\UserModel;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as Doc;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Doc\Tag('user')]
#[Route('/user', methods: ['GET'])]
#[Doc\Response(response: 400, description: 'Wrong request parameters', content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
#[Doc\Response(response: 401, description: "Invalid JWT token", content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
#[Doc\Response(response: 403, description: "Access denied", content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
#[Security(name: "Bearer")]
final class UserQueryController extends AbstractController
{
    #[Route, Doc\Get(summary: 'Get current user information',
        responses: [
            new Doc\Response(response: 200, description: 'Current user information', content: new Doc\JsonContent(ref: new Model(type: UserModel::class)))
        ]
    )]
    public function viewAction(): JsonResponse
    {
        return $this->json(
            new UserModel($this->getUser()),
            Response::HTTP_OK
        );
    }
}
