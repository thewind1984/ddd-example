<?php

declare(strict_types=1);

namespace App\User\Presentation\Api\Rest\Version1\Open;

use App\User\Application\UserCommandProcessor;
use App\User\Domain\Dto\CreateUserDto;
use OpenApi\Attributes as Doc;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Doc\Tag('user')]
#[Route('/user')]
#[Doc\Response(response: 400, description: 'Wrong request parameters', content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
final class UserCommandController extends AbstractController
{
    #[Route('/signup', methods: ['POST']), Doc\Post(summary: 'Register new user',
        requestBody: new Doc\RequestBody(content: new Doc\JsonContent(required: ['email', 'password'], properties: [
            new Doc\Property(property: 'email', type: 'string'),
            new Doc\Property(property: 'password', type: 'array', items: new Doc\Items(properties: [
                new Doc\Property(property: 'first', description: 'Password', type: 'string'),
                new Doc\Property(property: 'second', description: 'Password confirmation', type: 'string'),
            ])),
        ])),
        responses: [
            new Doc\Response(response: 201, description: 'User registered, verification email sent', content: null)
        ]
    )]
    public function signUpAction(Request $request, UserCommandProcessor $commandProcessor): JsonResponse
    {
        // service->method(
        // (service)(arg1, arg2)
        $password = $request->get('password');

        $commandProcessor->createUser(
            new CreateUserDto(
                $request->get('email'),
                $password['first'] ?? '',
                $password['second'] ?? '',
                $request->getClientIp(),
                $request->getLocale(),
            )
        );

        return $this->json(
            null,
            Response::HTTP_CREATED
        );
    }
}
