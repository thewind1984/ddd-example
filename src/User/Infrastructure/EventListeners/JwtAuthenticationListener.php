<?php

declare(strict_types=1);

namespace App\User\Infrastructure\EventListeners;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authenticator\Token\JWTPostAuthenticationToken;
use App\User\Application\JWTTokenProcessor;
use App\User\Domain\Dto\CreateJWTTokenDto;
use App\User\Domain\Entities\User;
use App\User\Domain\Events\UserSignedIn;
use App\User\Domain\Exceptions\JwtTokenIsInvalidException;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\RouterInterface;

#[AsEventListener(event: 'lexik_jwt_authentication.on_authentication_success', method: 'onJWTIssuedSuccessfully')]
#[AsEventListener(event: 'lexik_jwt_authentication.on_jwt_authenticated', method: 'onJWTAuthenticated')]
#[AsEventListener(event: 'lexik_jwt_authentication.on_jwt_created', method: 'onJWTCreated')]
final readonly class JwtAuthenticationListener
{
    public function __construct(
        private JWTTokenProcessor $JWTTokenProcessor,
        private RequestStack $requestStack,
        private RouterInterface $router,
        private EntityManagerInterface $em,
        private MessageBusInterface $messenger,
        private int $tokenTtl
    ) {}

    /**
     * When JWT is extracted from headers and parsed
     *
     * @throws JwtTokenIsInvalidException
     */
    public function onJWTAuthenticated(JWTAuthenticatedEvent $event): void
    {
        /** @var JWTPostAuthenticationToken $token */
        $token = $event->getToken();

        $jwtToken = $this->JWTTokenProcessor->getByUserAndJWTString(
            $event->getToken()->getUser(),
            $token->getCredentials()
        );

        if ($event->getToken()->getUser()->isDeleted()) {
            $jwtToken->invalidate();
        }

        if ($jwtToken !== null && !$jwtToken->isValid()) {
            throw new JwtTokenIsInvalidException();
        }

        $this->em->wrapInTransaction(
            function () use ($event): void {
                $event->getToken()->getUser()->login();
            }
        );

        if ($this->isRouteEqualsTo('user_logout')) {
            $this->JWTTokenProcessor->invalidateToken($token->getCredentials());
        }
    }

    /**
     * When JWT is in process of creation, we add extra payload
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $payload = $event->getData();

        // here we can add anything to payload of token
        // ...

        $event->setData($payload);
    }

    /**
     * When JWT token is issued for the first time
     * When JWT token is issued after refreshing
     */
    public function onJWTIssuedSuccessfully(AuthenticationSuccessEvent $event): void
    {
        /** @var User $user */
        $user = $event->getUser();

        $jwtToken = $event->getData()['token'];
        $request = $this->requestStack->getCurrentRequest();

        if ($this->JWTTokenProcessor->getByUserAndJWTString($user, $jwtToken) === null) {
            $this->JWTTokenProcessor->createToken(new CreateJWTTokenDto(
                $user,
                $jwtToken,
                $this->tokenTtl,
                $request->getClientIp(),
                $request->headers->get('User-Agent')
            ));
        }

        $data = $event->getData();
        $data['expiresIn'] = $this->tokenTtl;
        $event->setData($data);

        if (!$this->isRouteEqualsTo('gesdinet_jwt_refresh_token')) {
            // JWT token is issued for the first time (not refreshed!)
            $this->messenger->dispatch(new UserSignedIn($user->getId(), $request->getClientIp(), $request->getLocale()));
        }
    }

    private function isRouteEqualsTo(string $routeName): bool
    {
        try {
            $routeMatches = $this->router->match($this->requestStack->getCurrentRequest()->getRequestUri());

            return $routeMatches['_route'] === $routeName;
        } catch (\Throwable $e) {
            return false;
        }
    }
}
