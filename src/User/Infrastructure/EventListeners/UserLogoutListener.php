<?php

declare(strict_types=1);

namespace App\User\Infrastructure\EventListeners;

use App\User\Application\JWTTokenProcessor;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Security\Http\Event\LogoutEvent;

#[AsEventListener(event: 'Symfony\Component\Security\Http\Event\LogoutEvent')]
final readonly class UserLogoutListener
{
    public function __construct(
        private JWTTokenProcessor $JWTTokenProcessor,
    ) {}

    // Logout - expire JWT (extracted from attribute of backend token)
    public function __invoke(LogoutEvent $event): void
    {
        $jwtToken = $event->getToken()->hasAttribute('jwt_token') ? $event->getToken()->getAttribute('jwt_token') : null;
        if ($jwtToken !== null) {
            $this->JWTTokenProcessor->invalidateToken($jwtToken);
        }
    }
}
