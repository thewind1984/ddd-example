<?php

declare(strict_types=1);

namespace App\User\Infrastructure\EventListeners;

use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;

#[AsEventListener(event: 'Symfony\Component\Security\Http\Event\LoginSuccessEvent')]
final readonly class UserLoginListener
{
    // Login - save JWT as an attribute to backend token
    public function __invoke(LoginSuccessEvent $event): void
    {
        $request = $event->getRequest()->request;
        if ($request->has('jwt_token')) {
            $event->getAuthenticatedToken()->setAttribute('jwt_token', $request->get('jwt_token'));
        }
    }
}
