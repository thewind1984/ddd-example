<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Persistence;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\Persistence\ManagerRegistry;
use App\User\Domain\Entities\JWTToken;
use App\User\Domain\Entities\User;
use App\User\Domain\Interfaces\JWTTokenReadStorage;
use App\User\Domain\Interfaces\JWTTokenWriteStorage;

final class JWTTokenDoctrineRepository extends ServiceEntityRepository implements JWTTokenReadStorage, JWTTokenWriteStorage
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JWTToken::class);
    }

    public function getByUser(User $user): array
    {
        return $this->findBy([
            'user' => $user,
        ], ['issuedAt' => 'desc']);
    }

    public function get(string $jwtString, bool $lock = false): ?JWTToken
    {
        $token = $this->findOneBy(['token' => $jwtString]);

        if ($token !== null && $lock) {
            $this->_em->lock($token, LockMode::PESSIMISTIC_WRITE);
        }

        return $token;
    }

    public function getByUserAndJWTString(User $user, string $jwtString): ?JWTToken
    {
        return $this->findOneBy([
            'user' => $user,
            'token' => $jwtString,
        ]);
    }

    public function add(JWTToken $JWTToken): void
    {
        $this->_em->persist($JWTToken);
    }
}
