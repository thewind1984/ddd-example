<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Persistence\Mapping;

enum UserStatusType: string
{
    case NEW = 'new';
    case VERIFIED = 'verified';
}
