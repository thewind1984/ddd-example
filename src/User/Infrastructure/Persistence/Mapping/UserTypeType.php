<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Persistence\Mapping;

enum UserTypeType: string
{
    case USER = 'user';
    case ADMIN = 'admin';
}
