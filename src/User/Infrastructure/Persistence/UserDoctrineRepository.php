<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Persistence;

use App\User\Domain\Entities\User;
use App\User\Domain\Interfaces\UserReadStorage;
use App\User\Domain\Interfaces\UserWriteStorage;
use App\User\Infrastructure\Persistence\Mapping\UserStatusType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class UserDoctrineRepository extends ServiceEntityRepository
    implements UserReadStorage, UserWriteStorage, UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function loadUserByIdentifier(string $identifier): ?UserInterface
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :identifier')
            ->andWhere('u.deletedAt IS NULL')
            ->andWhere('u.status = :verified')
            ->setParameters([
                'identifier' => $identifier,
                'verified' => UserStatusType::VERIFIED,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function add(User $user): void
    {
        $this->_em->persist($user);
    }

    public function get(UuidInterface $id, bool $lock = false): ?User
    {
        $user = $this->find($id);

        if ($user !== null && $lock) {
            $this->_em->lock($user, LockMode::PESSIMISTIC_WRITE);
        }

        return $user;
    }

    public function getUnverifiedByCode(string $code): ?User
    {
        return $this->findOneBy([
            'status' => UserStatusType::NEW,
            'verificationCode' => $code,
        ]);
    }
}
