<?php

declare(strict_types=1);

namespace App\User\Infrastructure\EventHandlers;

use App\User\Domain\Events\UserSignedIn;
use App\User\Domain\Interfaces\UserReadStorage;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Mime\Email;
use Symfony\Component\Translation\LocaleSwitcher;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

#[AsMessageHandler]
final readonly class UserSignedInHandler
{
    public function __construct(
        private UserReadStorage $userReadStorage,
        private MailerInterface $mailer,
        private Environment $twig,
        private TranslatorInterface $translator,
        private LocaleSwitcher $localeSwitcher,
        private string $appContactEmail,
    ) {}

    public function __invoke(UserSignedIn $event): void
    {
        $user = $this->userReadStorage->get($event->userId);

        $this->localeSwitcher->runWithLocale($event->locale, function () use ($user, $event) {
            $this->mailer->send((new Email())
                ->from($this->appContactEmail)
                ->to($user->getEmail())
                ->subject($this->translator->trans('user.signing.subject', domain: 'emails'))
                ->html($this->twig->render('email/user/signin.html.twig', [
                    'userEmail' => $user->getEmail(),
                    'userIp' => $event->userIp,
                ]))
            );
        });
    }
}
